/* request full screen; see https://stackoverflow.com/a/7525760 */
function requestFullScreen(element) {
    // Supports most browsers and their versions.
    var requestMethod = element.requestFullScreen ||
                        element.webkitRequestFullScreen ||
                        element.mozRequestFullScreen ||
                        element.msRequestFullScreen;

    if (requestMethod) { // Native full screen.
        requestMethod.call(element);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
}

/* lecturer start */
var displayUI = true;

/* toggle user interface and fullscreen with tabulation key */
document.addEventListener("keydown", function(e){
    if (e.keyCode === 9){ // Tab
        if (displayUI){
            document.querySelectorAll("button").forEach(function(button){
                button.style.display = "none";
            });
            if (document.getElementById("menu")){
                document.getElementById("menu").style.display = "none";
            }
            document.getElementById("message").style.display = "none";
            displayUI = false;
            requestFullScreen(document.documentElement);
        }else{
            document.querySelectorAll("button").forEach(function(button){
                button.style.display = "initial";
            });
            if (document.getElementById("menu")){
                document.getElementById("menu").style.display = "initial";
            }
            document.getElementById("message").style.display = "initial";
            displayUI = true;
        }
    }
}, false);

/* buttons and messages */
if (document.getElementById("title")) { // we're home
}else{
    home = document.createElement("button");
    home.id = "home";
    home.title = "Accueil";
    home.innerHTML = "<i class='fas fa-home'>";
    home.addEventListener("click", function(){
        socket.emit("goHome");
    }, false);
    var buttonZone = document.getElementById("button");
    buttonZone.insertBefore(home, buttonZone.firstChild);

    var goHome = socket.on("goHome", function() {
        window.location.href = "/@lecturer-key";
    });
}

message = document.createElement("div");
message.id = "message";
document.body.appendChild(message);

function plural(num){ return (num > 1 ? "s" : ""); }

var connectionStats = socket.on("connectionStats", function(data){
    message.textContent =
        data.lecturers + " conférencier" + plural(data.lecturers) + ", " + 
        data.listeners + " auditeur" + plural(data.listeners) + "\n" +
        "pic de connexion : " + data.peak + " participant" + plural(data.peak);
});

var btnChangeColor = document.getElementById("toggle-backgroundcolor");
var bool = true;
btnChangeColor.addEventListener("click", function(){
    document.body.classList.toggle("changeBody");
    btnChangeColor.classList.toggle("changeBtn");
    if(bool === true){
        btnChangeColor.textContent = "Mode Blanc";
    } else {
        btnChangeColor.textContent ="Mode Noir";
    }
    bool = !bool;
    document.querySelector("li.tool").style.display = "block";
})
/* lecturer end */

/* listener start */
document.addEventListener("click", function(){
    requestFullScreen(document.documentElement);
}, false);

document.addEventListener("keypress", function(){
    requestFullScreen(document.documentElement);
}, false);

if (document.getElementById("title")) { // we're home
}else{
    var goHome = socket.on("goHome", function() {
        window.location.href = "/";
    });
}

/* listener end */
