/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

var term = new Terminal({
    "letterSpacing": 0,
    "fontFamily": "monospace"
});

term.open(document.getElementById('terminal'));

var socket = io({ auth: { token: "@lecturer-key" } });

var initTerm = socket.on("initTerm", function(data) {
    var checkFontSize = document.getElementById("checkFontSize");
    var fontAspectRatio = checkFontSize.getBoundingClientRect().width/
                          checkFontSize.getBoundingClientRect().height;
    var fontHeight = Math.min(
            window.screen.availHeight/data.nl,
            window.screen.availWidth/data.nc/fontAspectRatio);
    term.setOption("fontSize", fontHeight);
    term.resize(data.nc, data.nl);

    /* readjust because above mechanism does not take into account interletter
     * and interline spacing, hoping that they are proportional to font size */
    var viewport = document.querySelector('.xterm-viewport');
    fontHeight = Math.min(
            window.screen.availHeight/viewport.clientHeight,
            window.screen.availWidth/viewport.clientWidth)*fontHeight;
    term.setOption("fontSize", fontHeight);
    term.resize(data.nc, data.nl);
});

var updateTerminal = socket.on("updateTerminal", function(data) {
    term.write(data);
});
