/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

var slideNum,
    firstFetchSlide,
    currFetchSlide,
    numberOfSlides;


function slideFromNum(num) {
    return "slides/slide_" + num + ".svg|";
};

function displaySlide() {
    if (currFetchSlide !== firstFetchSlide){
        document.getElementById("prefetch").removeEventListener("load",
                fetchNextSlide);
    };
    document.getElementById("slide").src = slideFromNum(slideNum);
};

function fetchNextSlide() {
    currFetchSlide = (currFetchSlide % numberOfSlides) + 1;
    if (currFetchSlide === firstFetchSlide){
        document.getElementById("slide").removeEventListener("load",
            fetchNextSlide);
        return;
    }
    document.getElementById("prefetch").addEventListener("load",
            fetchNextSlide, { once: true });
    document.getElementById("prefetch").src = slideFromNum(currFetchSlide);
};

var socket = io({ auth: { token: "@lecturer-key" } });

var firstSlide = socket.on("firstSlide", function(data) {
    slideNum = data.num;
    numberOfSlides = data.max;
    firstFetchSlide = slideNum;
    currFetchSlide = slideNum;
    document.getElementById("slide").addEventListener("load", fetchNextSlide);
    displaySlide();
});

var goToSlide = socket.on("goToSlide", function(num) {
    slideNum = num;
    displaySlide();
});


/* lecturer start */
document.getElementById("add-empty-slide").addEventListener("click", function(){
    socket.emit("addEmptySlide", Tools.boardName);
}, false);
document.getElementById("clean-slide").addEventListener("click", function(){
    socket.emit("cleanSlide", Tools.boardName);
}, false);

document.getElementById("clean-slides").addEventListener("click", function(){
    socket.emit("cleanAllSlides");
}, false);

document.addEventListener("keydown", function(e){
    switch (e.keyCode) {
    case 37: // left
        socket.emit("incrSlide", -1);
        break;
    case 40: // down
        socket.emit("incrSlide", 10);
        break;
    case 38: // up
        socket.emit("incrSlide", -10);
        break;
    case 39: // right
        socket.emit("incrSlide", 1);
        break;
    case 33: // page up
        socket.emit("prevSlide");
        break;
    case 34: // page up
        socket.emit("lastSlide");
        break;
    }
}, false);

const btnAdd = document.getElementById("add-slide");
const btnSubmit = document.getElementById('buttonSubmit');
const form = document.querySelector('form');
const orgFile = btnAdd.files[0];


btnSubmit.addEventListener('click', (e) => {
    e.preventDefault();
    btnAdd.click();
});
btnAdd.addEventListener("change", async function(e){
    // validation uploaded file
    if( fileValidation(btnAdd) === false){
        return;
    } 
    await socket.emit("resetUploadFolder");
    if(getExtension(btnAdd.files[0].name) === 'pdf' ){
        readPDF(e.srcElement.files[0]);
    } else {
        readFile(e.srcElement.files[0]);
    }  
    
}, false);

//transfer content file SVG by type text
function readFile(file) {                                                       
    var reader = new FileReader();
    reader.onload = readSuccess;                                            
     function readSuccess(evt) { 
        var field = document.getElementById('main');                        
        field.innerHTML = evt.target.result;
        socket.emit("transferFile", {
                files: evt.target.result, 
                name: btnAdd.files[0].name, 
                ext: getExtension(btnAdd.files[0].name),
            }); 
        setTimeout(()=>{
            socket.emit("addNewSlide");
        }, 100);
    };
    reader.readAsText(file);                                              
}

//Tranfer content file PDF by type base64
function readPDF(file){
    var fileReader = new FileReader();  
    fileReader.readAsDataURL(file);
	fileReader.onload = function(e) {
        var field = document.getElementById('main');                        
        field.innerHTML = e.target.result;
        Toast.show("Veuillez attendre 5-6 secondes pour traiter le fichier.", "", 6000);
		socket.emit("transferFile", {
            files: e.target.result, 
            name: btnAdd.files[0].name, 
            ext: getExtension(btnAdd.files[0].name),
        }); 
        setTimeout(()=>{
            socket.emit("addNewSlideFromPDF");
        }, 6000);
        setTimeout(()=>{
            Toast.show("Le fichier est enregistré avec succès!", "success", 2000);
	    }, 6500);
    };
}


function fileValidation(fi) {
    //check size of file
    if (fi.files.length > 0) {
        for (let i = 0; i <= fi.files.length - 1; i++) {
            let fsize = fi.files.item(i).size;
            let file = Math.round((fsize / 1024));
            // The size of the file.
            if (file >= 4096) {
                alert(
                  "File too big, please select a file less than 4mb");
                return false;
                }
        }
    }

    //check extentions of file
    let filePath = fi.value;
        // var allowedExtensions = /(\.doc|\.docx|\.odt|\.pdf|\.tex|\.txt|\.rtf|\.wps|\.wks|\.wpd)$/i;
    const allowedExtensions = /(\.svg|\.pdf|)$/i;                  
    if (!allowedExtensions.exec(filePath)) {
        alert('Just valide file type SVG');
        fi.value = '';
        return false;
    }          

}

function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

// Message wait for loading file
const Toast = {
    init(){
        this.hideTimeout = null;
        this.el = document.createElement("div");
        document.body.appendChild(this.el);
    },
    show(message, state, time){
        clearTimeout(this.hideTimeout);
        this.el.textContent = message;
        this.el.className = "toast toast--visible";
        if(state){
            this.el.classList.add(`toast--${state}`);
        }
        this.hideTimeout = setTimeout(()=>{
            this.el.classList.remove('toast--visible');
        }, time);
    }
};

document.addEventListener("DOMContentLoaded", ()=> Toast.init());
/* lecturer end */

