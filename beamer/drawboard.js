/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

document.getElementById("slide").addEventListener("load", function (){
    document.getElementById("drawingArea").replaceChildren();
    var slide = document.getElementById("slide");
    Tools.SVGVirtualHeight = Tools.SVGVirtualWidth*
        slide.naturalHeight/slide.naturalWidth;
    Tools.svg.viewBox.baseVal.height = Tools.SVGVirtualHeight;
    Tools.boardName = "slide_" + slideNum;
    socket.emit("getDrawboard", Tools.boardName);

});


/* lecturer start */
Tools.change("Pencil");
Tools.setColor(Tools.colorPresets[1].color); // Reddish
Tools.setSize(Tools.server_config.MAX_TOOL_SIZE/5);
Tools.HTML.addShortcut(";", function(){ /* black pencil */
    if (Tools.curTool.name !== "Pencil"){
        Tools.change("Pencil");
        document.activeElement.blur && document.activeElement.blur();
    }
    Tools.setColor(Tools.colorPresets[0].color); // Blackish
    Tools.setOpacity(1);
    Tools.setSize(Tools.server_config.MAX_TOOL_SIZE/12);
});

Tools.HTML.addShortcut(":", function(){ /* highlighter */
    if (Tools.curTool.name !== "Pencil"){
        Tools.change("Pencil");
        document.activeElement.blur && document.activeElement.blur();
    }
    Tools.setColor(Tools.colorPresets[4].color); // Yellowish
    Tools.setOpacity(0.5);
    Tools.setSize(Tools.server_config.MAX_TOOL_SIZE/2);
});

Tools.HTML.addShortcut("!", function(){ /* red pointer */
    Tools.change("Pointer");
    document.activeElement.blur && document.activeElement.blur();
    Tools.setColor(Tools.colorPresets[1].color); // Reddish
    Tools.setOpacity(.8);
    Tools.setSize(Tools.server_config.MAX_TOOL_SIZE/5);
});
/* lecturer end */
