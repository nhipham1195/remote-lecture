# Remote Lecture: light and free software for visual support of live online lectures.

Very simple Node.js server with three tools, broadcasting quality visual support for live online lectures using as few network resources as possible; in particular avoiding the use of videos.  

 * [Editable beamer slideshow](#editable-beamer): live broadcast of slides, which can be edited with simple drawing tools. Users of stylus can display blank slides and use them as a series of small, fixed whiteboards. Drawing tools are built upon source code of the online collaborative whiteboard [WBO](https://github.com/lovasoa/whitebophir).

 * [Formatted textboard](#formatted-textboard): live display of text, possibly formatted with HTML tags, including source code formatting with [Prism](https://prismjs.com/) and math display with [jqMath](https://mathscribe.com/author/jqmath.html).

 * [Virtual terminal](#virtual-terminal): live display of a virtual terminal, using [Xterm.js](https://github.com/xtermjs/xterm.js).

All interactions and communications use [socket.io](https://socket.io/).  

These tools are designed to be used **in conjunction with a voice over IP software**. Light and free solutions readily exist, see in particular [Mumble](https://www.mumble.info/).  

## Startup

Install socket.io  

    npm install socket.io   

Run the server  

    node server/server.js  

By default, the server should be accessible at address
`http://localhost:30000/`  

## Documentation

Configuration file `server/configuration.js`  

### Lecturers and Listeners

Lecturers must access the server by adding the lecturer key (by default, `lecturer`) to the root address, for instance `http://localhost:30000/lecturer`.  

There can be several lecturers, each having the ability to control what is displayed to every connected browsers : navigating through the available tools, or browsing and editing the beamer slideshow.  

Navigation buttons are currently in french: Diaporama = Beamer, Tableau de texte = Textboard, Accueil = Home.  

Listeners must only access the server by its root address. They will automatically be redirected to the tool currently being used by the lecturer.  

Listeners cannot interact with the tools. They are encouraged to let their display fullscreen, which can be achieved by pressing any key or mouse button.  

Lecturers can toggle buttons visibility with tabulation key. Hiding buttons this way puts the display fullscreen.

Connexion statistics (current number of lecturers and listeners connected, and maximum number of connexions reached) are displayed to lecturers when not fullscreen, and logged by default in `server/connection_stats.log`.

### Editable Beamer

Currently, slides must be in SVG format and put in directory `beamer/slides`, with names formatted as `slide_%d.svg`, where `%d` is the slide number.  

For conversion to SVG from PDF, [pdf2svg](http://cityinthesky.co.uk/opensource/pdf2svg/) can be useful

    pdf2svg <pdf_file> "slide_%d.svg" all

Resulting files sizes can be reduced with [SVG cleaner](https://github.com/RazrFalcon/SVGCleaner)

    for file in *.svg; do svgcleaner $file $file; done

Browse one slide at a time with `left` and `right` arrows.
Browse ten slides at a time with `up` and `down` arrows.
Jump to last slide with `page down`.
Jump to previous slide with `page up`.  

Each slide has its own set of annotations: annotations on a slide do not appear on the other slides but they are remembered when browsing back to this slide.

Drawing tools/secondary function (activated with `shift` key) and shortcut:

 - Pencil/White-out (hand drawing) `p`
 - Pointer (hand drawing disappearing after some time) `o`
 - Free/prefined angles Straight Lines `l`
 - Rectangles/Squares `r`
 - Ellipses/Circles `c`
 - Text `t`
 - Eraser `e`
 - Predefined colors `0-9`
 - Small opaque dark pencil `;`
 - Thick semi-transparent yellow highlighter `:` 
 - Small slightly transparent red pointer `!`

Additional buttons:

 - Ajouter diapositive vierge = Add blank slide: add a blank slide to the end of the slideshow and jump to this slide
 - Nettoyer diapositive = Clean slide: delete all annotations on the current slide
 - Nettoyer diaporama = Clean slideshow: delete all annotations on all slides

### Formatted Textboard

The textboard displays the content of the files `textboard/textboard(0|1).tbd` in two columns.

The files are interpreted as HTML, and can thus contain any standard tag. A special `<pw>` tag is styled with `white-space: pre-wrap`, allowing to freely write spaces and newlines without the need for explicit breaks and new paragraphs.

Mathematics can be written on the fly using [jqMath syntax](https://mathscribe.com/author/jqmath.html).
In particular, anything within dollar sign delimiters will be typeset in math mode, and there is an interpreter mechanism for subscripts and superscripts, bold and script letters, and simple table layout.
For greek letters and special symbols, use [HTML entities](https://www.w3schools.com/charsets/ref_utf_math.asp) or any unicode character directly within the file.

Source code syntax coloring can be achieved with [Prism](https://prismjs.com/), using `<code>` tags. Languages are enabled by putting corresponding Prism components in `textboard/prism` and by loading them in`textboard/textboard.html`
(currently by default, only C and a custom pseudocode).

The lecturer must write directly to the files, either by physical access to the server or by some tunneling connection.
Note that for actual real-time broadcast, the text editor must write each modification to the file.
It is advised to use a dual monitor for displaying both the text editor and the resulting textboard on the browser. 

See my [Vim plugin](https://gitlab.com/1a7r0ch3/vim-config/-/blob/master/ftplugin/textboard.vim) enabling quick and smooth use of the tool (though some training is required).

### Virtual Terminal

Broadcast a virtual terminal by dumping its raw output to a [named pipe](https://en.wikipedia.org/wiki/Named_pipe) on the server (by default `terminal/pipe`).

On Unix, use the `mkfifo` and `script` utilities.

Create the pipe on the server

    mkfifo terminal/pipe

Open a virtual terminal and broadcast it with 

    script -f terminal/pipe

If no physical access to the server is available, one must use a local named pipe and send its output to the server's named pipe through some tunneling connection. For instance with SSH

    mkfifo local_pipe
    cat local_pipe | ssh <server> 'cat > path/to/terminal/pipe'&
    script -f local_pipe

Ensure terminal numbers of lines and columns fit the configuration used (by default, 44×159).
Listener experience better in fullscreen and browser zoom at 100 %.

Tested with xterm and urxvt.

## References

## License
This software is under the GPLv3 license.
