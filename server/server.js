/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

const http = require('http');
const path = require('path');
const config = require('./configuration.js');
const staticFiles = require('./static.js');
const beamer = require('./beamer.js');
const textboard = require('./textboard.js');
const terminal = require('./terminal.js');
const sockets = require('./sockets.js');

const server = http.createServer(function (request, response) {
    var isLecturer = request.url.match(config.LECTURER_KEY);

    if (staticFiles.serve(request.url, response, isLecturer)) { return; }
    
    if (beamer.serve(request.url, response)) { return; }
    
    console.log("Resource not found: " + request.url);
    response.writeHead(404, {"Content-Type": "text/html"});
    response.end("Not Found");
});

sockets.start(server);

server.listen(config.PORT);

console.log("Serving '" + config.TITLE + "'");
console.log("Port: " + config.PORT);
console.log("Lecturer key: " + config.LECTURER_KEY);
console.log(beamer.info);
console.log(textboard.info);
console.log(terminal.info);
