/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

const fs = require('fs');
const base64 = require('base64topdf');
const path = require('path');
const drawboard = require('./drawboard.js');
const config = require('./configuration.js');
const SLIDES_DIR = "beamer/slides";
const UPLOADSLIDES_DIR = "beamer/uploadslide/"
const { exec } = require("child_process");
let contentUploadedFile;

/* default empty slide (4:3 aspect ratio) */
function emptySlide(){
    return '<svg width="4" height="3" viewBox="0 0 4 3" ' + 
                'xmlns="http://www.w3.org/2000/svg" ' + 
                'xmlns:xlink="http://www.w3.org/1999/xlink">' +
                '<rect width="4" height="3" style="fill:white;stroke-width:0"/>' +
            '</svg>';
}

/* add empty slide at the end */
function pushEmptySlide(){
    slides.push(emptySlide());
    numberOfSlides += 1;
}

/* store uploaded file to a folder in server **/
async function storeUploadFile(files){
    let filePath =  SLIDES_DIR + '/slide_' + (numberOfSlides+1) + '.svg';
    var mainContent;
    if (files.ext === 'svg'){
        mainContent = checkViewBox(files.files);
    } else {
        mainContent = files.files;
    }
    fs.writeFile(filePath, mainContent, function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
        contentUploadedFile = fs.readFileSync(filePath, "UTF-8");
    });       
}

/* default add slide  */
function addSlide(){  
    return contentUploadedFile;
}

/* counting beamer slides */
var prevSlideNum = 1,
    slideNum = 1;
    numberOfSlides = findMaxNum(fs.readdirSync(SLIDES_DIR).
    filter(fname => fname.match(/slide_(\d+)\.svg/)));

function findMaxNum(arr){
    let listNumFile= [];
    for(i=0; i<arr.length; i++){
        listNumFile[i] = Number(arr[i].split('_')[1].split('.')[0]);
    }
    return Math.max(...listNumFile);
}

function incrSlide(n, m) {
    if(m === 0){
        prevSlideNum = slideNum;
        slideNum = slideNum + n;
    } else {
        slideNum = m + n;
    }
    if (slideNum < 1) slideNum = 1;
    else if (slideNum > numberOfSlides) slideNum = numberOfSlides;
}

function lastSlide(){
    prevSlideNum = slideNum;
    slideNum = numberOfSlides;
}

function prevSlide(){
    slideNum = prevSlideNum;
}

/* add empty slide at the end */
function pushNewSlide(){
    slides.push(addSlide());
    numberOfSlides += 1;
}

/* preload beamer slides in memory */
var missingSlides = [];
var slides = [];
if (numberOfSlides === 0){
    pushEmptySlide()
}else{
    for (let num = 1; num <= numberOfSlides; num++){
        var slide = path.join(SLIDES_DIR, "slide_" + num + ".svg");
        if (!fs.existsSync(slide)) {
            missingSlides.push(num);
            slides.push(emptySlide());
        }else{
            slides.push(fs.readFileSync(slide, "UTF-8"));
        }
    }
}

/* Reset the folder uploadslide before receiving more file*/
function resetUploadFolder(){
    var directory = "beamer/uploadslide/";
    fs.readdir(directory, (err, files) => {
        if (err) throw err;
        
        for (const file of files) {
            fs.unlink(path.join(directory, file), err => {
            if (err) throw err;
            });
        }
        });
}
 /* add uploaded files at the end */
function addSlideFromPDF(){
    var numberOfUploadSlides = fs.readdirSync(UPLOADSLIDES_DIR).
        filter(fname => fname.match(/uploadedslide_(\d+)\.svg/)).length;
        let oldNmbSlide = numberOfSlides;
        numberOfSlides += numberOfUploadSlides;

    // Add to memory: slides[]
    for (let num = 1; num <= numberOfUploadSlides; num++){
        var slide = path.join(UPLOADSLIDES_DIR, "uploadedslide_" + num + ".svg");
        if (!fs.existsSync(slide)) {
            missingSlides.push(num);
            slides.push(emptySlide());
        }else{
            slides.push(fs.readFileSync(slide, "UTF-8"));
        }
    }

    // Move files from uploadslide -> slides
    for (let i=oldNmbSlide+1; i<=numberOfSlides; i++){
        var oldPath = path.join(UPLOADSLIDES_DIR, "uploadedslide_" + (i-oldNmbSlide) + ".svg");
        var newPath = path.join(SLIDES_DIR, "slide_" + i + ".svg");
        fs.rename(oldPath, newPath, () => {
            console.log('Move File');
        });      
    }
    return oldNmbSlide;
 };


/* Check viewbox of file SVG */
function checkViewBox(file){
    if (file.includes("viewBox") !== true){
        var indexAdd = file.indexOf('svg') + 4;
        var newContent =  file.slice(0, indexAdd) + ' viewBox="0 0 960 540" ' + file.slice(indexAdd);
        console.log('Uploaded file doesn\'t have viewBox, we\'ve just added property viewBox!');
        return newContent;
    } else {
        console.log('Uploaded file svg has viewBox');
        return file;
    }
}

/* serve beamer slides; return true if resource is found, false otherwise */
var slideRegExp = new RegExp(path.join(SLIDES_DIR, "slide_(\\d+)\\.svg"));
function serve(url, response) {
    var match = url.match(slideRegExp);
    if (match){
        response.writeHead(200, {
            "Content-Type": "image/svg+xml; text/pdf",
            "Cache-control": "max-age=" + config.beamer.CACHE_MAX_AGE 
                + ", must-revalidate"
        });
        response.end(slides[Number(match[1]) - 1]);
        return true;
    }else{
        return false;
    }
}

/***  communications throught sockets  ***/ 

/* broadcasting slide number */
function goToSlide(socket) {
    socket.emit("goToSlide", slideNum);
    socket.broadcast.emit("goToSlide", slideNum);
}

async function  onConnection(socket) {
    if (socket.handshake.auth.token === config.LECTURER_KEY){
       
        socket.on("incrSlide", function(n){
            incrSlide(n, 0);
            goToSlide(socket);
        });
        socket.on("lastSlide", function(){
            lastSlide();
            goToSlide(socket);
        });
        socket.on("prevSlide", function(){
            prevSlide();
            goToSlide(socket);
        });
        socket.on("addEmptySlide", function(){
            pushEmptySlide()
            lastSlide();
            goToSlide(socket);
        });
        socket.on("resetUploadFolder", function(){
            resetUploadFolder();
        });
        socket.on("transferFile", async function(files){
            await storeUploadFile(files);
            setTimeout(() =>{isImage(files, contentUploadedFile)}, 500);

        });
        socket.on("addNewSlide", function(){
            pushNewSlide();
            lastSlide();
            goToSlide(socket);
        });
        socket.on("addNewSlideFromPDF", async function(){
            let oldCurrentSlide = prevSlideNum;
            let position = (await addSlideFromPDF()) + 1 - oldCurrentSlide;
            incrSlide(position, oldCurrentSlide);       
            // reset all the uploaded files in the cache
            socket.emit("firstSlide", {
                num: slideNum,
                max: numberOfSlides
            });
            goToSlide(socket);
        });
        socket.on("cleanSlide", function(boardName){
            drawboard.handleMessage(boardName, { tool: "clean" });
            goToSlide(socket);
        });
        socket.on("cleanAllSlides", function(boardName){
            drawboard.handleMessage(boardName, { tool: "clean-all" });
            goToSlide(socket);
        });
        
        /* broadcasting drawboard messages */
        socket.on("broadcastDraw", function (message) {
            var boardName = message.board;
            var data = message.data;
            drawboard.handleMessage(boardName, data, socket);
            socket.broadcast.emit("broadcastDraw", data);
        });
    }
    
    socket.on("getDrawboard", async function (name) {
        var board = await drawboard.getBoard(name);
        socket.emit("broadcastDraw", { _children: board.getAll() });
    });

    /* start beamer with current slide */
    socket.emit("firstSlide", {
        num: slideNum,
        max: numberOfSlides
    });
}

const info = "Beamer slides are in\n  " +
    path.join(path.resolve("."), SLIDES_DIR) +
    (missingSlides.length === 0 ? "" :
     "\nWarning: slide(s) " + missingSlides + " missing;" +
     " replacing with blank slide(s)");



async function isImage(file, content) {
    var ext = file.ext;
    let filePath =  UPLOADSLIDES_DIR + file.name;
    switch (ext.toLowerCase()) {
    case 'pdf':
     
        //Convert type Base64 to type PDF
        var [type, contentFile] = content.split(',');
        let decodedBase64 = base64.base64Decode(contentFile, filePath);
        //Convert type PDF to type SVG by using command line + SVGCleaner
        let command = "cd beamer/uploadslide/ && " + 
                      "pdf2svg " +  file.name +  ' "uploadedslide_%d.svg" '  + "all &&" + 
                      "for file in *.svg; do svgcleaner $file $file; done";
        exec(command, (error, stdout, stderr) => {
                if (error) {
                    console.log(`error: ${error.message}`);
                    return;
                }
                if (stderr) {
                    console.log(`stderr: ${stderr}`);
                    return;
                }
                console.log(`stdout: ${stdout}`);
            });

    case 'svg':
    return true;
    }
    return false;
}

module.exports = { serve, onConnection, info };
