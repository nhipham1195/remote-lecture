/* Remote Lecture */
/* Copyright (C) 2021  Hugo Raguet */
/* SPDX-License-Identifier: GPL-3.0-or-later */

const fs = require('fs');
const path = require('path');
const config = require('./configuration.js');

const fd = fs.existsSync(config.terminal.PIPE) ?
    fs.openSync(config.terminal.PIPE, 'r+') :
    fs.openSync(config.terminal.PIPE + "_missing", 'a+');
const termStream = fs.createReadStream(null, { fd });
termStream.setEncoding('utf8');

var onStreamFunction = function () {};

function start(socket) {
    termStream.resume();
    termStream.on('data', onStreamFunction = function(data) {
        socket.emit("updateTerminal", data)
    });
}

function stop() {
    termStream.pause();
    termStream.removeListener('data', onStreamFunction);
}

function onConnection(socket){
    /* send terminal size */
    socket.emit("initTerm", {
        nc: config.terminal.COLUMNS,
        nl: config.terminal.LINES
    });
}

const info = fs.existsSync(config.terminal.PIPE) ?
    "Broadcast terminal " +
    "(" + config.terminal.LINES + "x" + config.terminal.COLUMNS + ") with\n" +
    "  `script -f " + path.join(path.resolve("."), config.terminal.PIPE) + "`"
    :
    "Terminal broadcasting requires a UNIX named pipe;\n" +
    "to use this functionality, restart the server after creating the pipe " + 
    "with\n" +
    "  `mkfifo " + config.terminal.PIPE + "`"

module.exports = { start, stop, onConnection, info };
